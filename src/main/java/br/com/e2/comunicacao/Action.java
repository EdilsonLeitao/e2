package br.com.e2.comunicacao;

public interface Action {
	void execute();
}
