package br.com.e2.core.audit;

import org.hibernate.envers.RevisionListener;

import br.com.e2.core.model.CustomRevisionEntity;

public class CustomEnversListener implements RevisionListener {
	@Override
	public void newRevision(Object revisionEntity) {

		CustomRevisionEntity customRevisionEntity = (CustomRevisionEntity) revisionEntity;
		try {
		} catch (Exception e) {
			   System.err.println(e.getMessage());
			   customRevisionEntity.setUsername("async_task");
		}
	}
}
