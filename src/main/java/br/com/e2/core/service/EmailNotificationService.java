package br.com.e2.core.service;


public interface EmailNotificationService {
	
	public void sendEmail(String emailOrigem, String emailDestino, String titulo, String corpo) throws Exception;
	
}
