package br.com.e2.core.service;

import java.util.List;


import javax.inject.Inject;
import javax.inject.Named;

import br.com.e2.core.json.*;
import br.com.e2.core.model.*;
import br.com.e2.core.persistence.DaoSyncInfo;

public interface SyncService {
		public DtoDataBase sync(DtoDataBase dataBase) ;
}
