
package br.com.e2.core.utils;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.e2.json.JsonDoacao;
import br.com.e2.json.JsonUsuario;
import br.com.e2.model.Doacao;
import br.com.e2.model.Usuario;

public class Parser {

	private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");
	private static final DateTimeFormatter HOUR_FORMAT = DateTimeFormat.forPattern("HH:mm");

	public static String getHourAsString(LocalDateTime date) {
		String format = "";
		try {
			format = HOUR_FORMAT.print(date);
		} catch (Exception e) {
			format = "00:00";
		}
		return format;
	}

	public static String getDateTimeAsString(LocalDateTime date) {
		String format = "";
		try {
			format = DATE_TIME_FORMAT.print(date);
		} catch (Exception e) {
			format = DATE_TIME_FORMAT.print(new DateTime());
		}
		return format;
	}

	public static String getDateAsString(LocalDateTime date) {
		String format = "";
		try {
			format = DATE_FORMAT.print(date);
		} catch (Exception e) {
			format = DATE_FORMAT.print(new DateTime());
		}
		return format;
	}

	//
	private static DateTime getHour(String date) {
		if (!date.isEmpty()) {
			try {
				return HOUR_FORMAT.parseDateTime(date);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static LocalDateTime getDate(String date) {
		if (!date.isEmpty()) {
			try {
				LocalDateTime dateTime = DATE_FORMAT.parseLocalDateTime(date);
				return dateTime;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static LocalDateTime getDateTime(String date) {
		if (!date.isEmpty()) {
			try {
				LocalDateTime dateTime = DATE_TIME_FORMAT.parseLocalDateTime(date);
				return dateTime;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	// Conversão de Entidade para Json Usuário
	private static JsonUsuario toBasicJson(Usuario usuario) {
		JsonUsuario jsonUsuario = new JsonUsuario();
		applyBasicJsonValues(jsonUsuario, usuario);
		return jsonUsuario;
	}

	private static Usuario toBasicEntity(JsonUsuario jsonUsuario) {
		Usuario usuario = new Usuario();
		applyBasicEntityValues(usuario, jsonUsuario);
		return usuario;
	}

	private static void applyBasicJsonValues(JsonUsuario jsonUsuario, Usuario usuario) {
		jsonUsuario.setId(usuario.getId());
		jsonUsuario.setNome(usuario.getNome());
		jsonUsuario.setPassword(usuario.getPassword());
		jsonUsuario.setImage(usuario.getImage());
		jsonUsuario.setEmail(usuario.getEmail());
	}

	private static void applyBasicEntityValues(Usuario usuario, JsonUsuario jsonUsuario) {
		usuario.setId(jsonUsuario.getId());
		usuario.setNome(jsonUsuario.getNome());
		usuario.setPassword(jsonUsuario.getPassword());
		usuario.setImage(jsonUsuario.getImage());
		usuario.setEmail(jsonUsuario.getEmail());
	}

	public static JsonUsuario toJson(Usuario usuario) {
		JsonUsuario jsonUsuario = new JsonUsuario();

		applyBasicJsonValues(jsonUsuario, usuario);

		/*List<Role> listRoles = usuario.getRoles();
		if (listRoles != null) {
			for (Role loopRole : listRoles) {
				jsonUsuario.getRoles().add(toBasicJson(loopRole));
			}
		}
		Empresa empresa_ = usuario.getEmpresa();
		if (empresa_ != null) {
			jsonUsuario.setEmpresa(toJson(empresa_));
		}*/
		return jsonUsuario;
	}

	public static Usuario apply(Usuario usuario, JsonUsuario jsonUsuario) {

		if (usuario == null)
			usuario = new Usuario();

		applyBasicEntityValues(usuario, jsonUsuario);

		/*ArrayList<JsonRole> listRoles = jsonUsuario.getRoles();
		if (listRoles != null) {
			for (JsonRole loopJsonRole : listRoles) {
				usuario.addRoles(toEntity(loopJsonRole));
			}
		}
		JsonEmpresa empresa_ = jsonUsuario.getEmpresa();
		if (empresa_ != null) {
			usuario.setEmpresa(toEntity(empresa_));
		}*/
		
		return usuario;
	}

	public static Usuario toEntity(JsonUsuario jsonUsuario) {
		Usuario usuario = new Usuario();

		return apply(usuario, jsonUsuario);
	}

	public static List<JsonUsuario> toListJsonUsuarios(List<Usuario> all) {
		List<JsonUsuario> jsonUsuarios = new ArrayList<JsonUsuario>();
		for (Usuario usuario : all) {
			jsonUsuarios.add(toJson(usuario));
		}
		return jsonUsuarios;
	}
	
	// Conversão de Entidade para Json Doação
	private static JsonDoacao toBasicJson(Doacao doacao) {
		JsonDoacao jsonDoacao = new JsonDoacao();
		applyBasicJsonValues(jsonDoacao, doacao);
		return jsonDoacao;
	}

	private static Doacao toBasicEntity(JsonDoacao jsonDoacao) {
		Doacao doacao = new Doacao();
		applyBasicEntityValues(doacao, jsonDoacao);
		return doacao;
	}

	private static void applyBasicJsonValues(JsonDoacao jsonDoacao, Doacao doacao) {
		jsonDoacao.setId(doacao.getId());
		jsonDoacao.setTitulo(doacao.getTitulo());
		jsonDoacao.setDescricao(doacao.getDescricao());
		jsonDoacao.setCategoria(doacao.getCategoria());
		jsonDoacao.setTipo(doacao.getTipo());
		jsonDoacao.setImg1(doacao.getImg1());
		jsonDoacao.setImg2(doacao.getImg2());
		jsonDoacao.setImg3(doacao.getImg3());
		jsonDoacao.setImg4(doacao.getImg4());
		jsonDoacao.setEndereco(doacao.getEndereco());
		jsonDoacao.setNumeroEndereco(doacao.getNumeroEndereco());
		jsonDoacao.setComplemento(doacao.getComplemento());
		jsonDoacao.setUf(doacao.getUf());
		jsonDoacao.setCidade(doacao.getCidade());
		jsonDoacao.setBairro(doacao.getBairro());
		jsonDoacao.setCep(doacao.getCep());
		jsonDoacao.setReferenciaEndereco(doacao.getReferenciaEndereco());
		jsonDoacao.setLatitude(doacao.getLatitude());
		jsonDoacao.setLongitude(doacao.getLongitude());
		jsonDoacao.setNome(doacao.getNome());
		jsonDoacao.setEmail(doacao.getEmail());
		jsonDoacao.setTelefone(doacao.getTelefone());
	}

	private static void applyBasicEntityValues(Doacao doacao, JsonDoacao jsonDoacao) {
		doacao.setId(jsonDoacao.getId());
		doacao.setId(jsonDoacao.getId());
		doacao.setTitulo(jsonDoacao.getTitulo());
		doacao.setDescricao(jsonDoacao.getDescricao());
		doacao.setCategoria(jsonDoacao.getCategoria());
		doacao.setTipo(jsonDoacao.getTipo());
		doacao.setImg1(jsonDoacao.getImg1());
		doacao.setImg2(jsonDoacao.getImg2());
		doacao.setImg3(jsonDoacao.getImg3());
		doacao.setImg4(jsonDoacao.getImg4());
		doacao.setEndereco(jsonDoacao.getEndereco());
		doacao.setNumeroEndereco(jsonDoacao.getNumeroEndereco());
		doacao.setComplemento(jsonDoacao.getComplemento());
		doacao.setUf(jsonDoacao.getUf());
		doacao.setCidade(jsonDoacao.getCidade());
		doacao.setBairro(jsonDoacao.getBairro());
		doacao.setCep(jsonDoacao.getCep());
		doacao.setReferenciaEndereco(jsonDoacao.getReferenciaEndereco());
		doacao.setLatitude(jsonDoacao.getLatitude());
		doacao.setLongitude(jsonDoacao.getLongitude());
		doacao.setNome(jsonDoacao.getNome());
		doacao.setEmail(jsonDoacao.getEmail());
		doacao.setTelefone(jsonDoacao.getTelefone());
	}

	public static JsonDoacao toJson(Doacao doacao) {
		JsonDoacao jsonDoacao = new JsonDoacao();

		applyBasicJsonValues(jsonDoacao, doacao);

/*		List<Punicao> listPunicaos = doacao.getPunicaos();
		if (listPunicaos != null) {
			for (Punicao loopPunicao : listPunicaos) {
				jsonDoacao.getPunicaos().add(toBasicJson(loopPunicao));
			}
		}
		List<MensagemDoacao> listMensagemDoacaos = doacao.getMensagemDoacaos();
		if (listMensagemDoacaos != null) {
			for (MensagemDoacao loopMensagemDoacao : listMensagemDoacaos) {
				jsonDoacao.getMensagemDoacaos().add(toBasicJson(loopMensagemDoacao));
			}
		}
		List<DoacaoBloqueado> listDoacaoBloqueados = doacao.getDoacaoBloqueados();
		if (listDoacaoBloqueados != null) {
			for (DoacaoBloqueado loopDoacaoBloqueado : listDoacaoBloqueados) {
				jsonDoacao.getDoacaoBloqueados().add(toBasicJson(loopDoacaoBloqueado));
			}
		}
		List<Corrida> listCorridas = doacao.getCorridas();
		if (listCorridas != null) {
			for (Corrida loopCorrida : listCorridas) {
				jsonDoacao.getCorridas().add(toBasicJson(loopCorrida));
			}
		}
		List<Plantao> listPlantoes = doacao.getPlantoes();
		if (listPlantoes != null) {
			for (Plantao loopPlantao : listPlantoes) {
				jsonDoacao.getPlantoes().add(toBasicJson(loopPlantao));
			}
		}
*/
		return jsonDoacao;
	}

	public static Doacao apply(Doacao doacao, JsonDoacao jsonDoacao) {

		if (doacao == null)
			doacao = new Doacao();

		applyBasicEntityValues(doacao, jsonDoacao);

		return doacao;

	}

	public static Doacao toEntity(JsonDoacao jsonDoacao) {
		Doacao doacao = new Doacao();

		return apply(doacao, jsonDoacao);
	}

	public static List<JsonDoacao> toListJsonDoacao(List<Doacao> all) {
		List<JsonDoacao> jsonDoacao = new ArrayList<JsonDoacao>();
		for (Doacao doacao : all) {
			jsonDoacao.add(toJson(doacao));
		}
		return jsonDoacao;
	}
}
