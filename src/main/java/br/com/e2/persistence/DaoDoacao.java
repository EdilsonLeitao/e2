package br.com.e2.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.e2.core.persistence.AccessibleHibernateDao;
import br.com.e2.core.persistence.pagination.Pagination;
import br.com.e2.core.persistence.pagination.PaginationParams;
import br.com.e2.core.persistence.pagination.Paginator;
import br.com.e2.model.Doacao;
import br.com.e2.model.Doacao;
import br.com.e2.model.filter.FilterDoacao;

@Named
@SuppressWarnings("rawtypes")
public class DaoDoacao extends AccessibleHibernateDao<Doacao> {

	private static final Logger LOGGER = Logger.getLogger(DaoDoacao.class);

	@Inject
	SessionFactory sessionFactory;

	public DaoDoacao() {
		super(Doacao.class);
	}

	
	@Override
	public Pagination<Doacao> getAll(PaginationParams paginationParams) {
		FilterDoacao filterDoacao = (FilterDoacao) paginationParams.getFilter();
		
		Criteria searchCriteria = criteria();
		Criteria countCriteria = criteria();

		if (filterDoacao.getTitulo() != null) {
			searchCriteria.add(Restrictions.ilike("titulo", filterDoacao.getTitulo(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("titulo", filterDoacao.getTitulo(), MatchMode.ANYWHERE));
		}

		if (filterDoacao.getDescricao() != null) {
			searchCriteria.add(Restrictions.ilike("descricao", filterDoacao.getDescricao(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("descricao", filterDoacao.getDescricao(), MatchMode.ANYWHERE));
		}
		
		if (filterDoacao.getCategoria() != null) {
			searchCriteria.add(Restrictions.ilike("categoria", filterDoacao.getCategoria(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("categoria", filterDoacao.getCategoria(), MatchMode.ANYWHERE));
		}

		if (filterDoacao.getTipo() != null) {
			searchCriteria.add(Restrictions.ilike("tipo", filterDoacao.getTipo(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("tipo", filterDoacao.getTipo(), MatchMode.ANYWHERE));
		}

		if (filterDoacao.getUf() != null) {
			searchCriteria.add(Restrictions.ilike("uf", filterDoacao.getUf(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("uf", filterDoacao.getUf(), MatchMode.ANYWHERE));
		}

		if (filterDoacao.getCidade() != null) {
			searchCriteria.add(Restrictions.ilike("cidade", filterDoacao.getCidade(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("cidade", filterDoacao.getCidade(), MatchMode.ANYWHERE));
		}

		if (filterDoacao.getBairro() != null) {
			searchCriteria.add(Restrictions.ilike("bairro", filterDoacao.getBairro(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("bairro", filterDoacao.getBairro(), MatchMode.ANYWHERE));
		}
		
		if (filterDoacao.getNome() != null) {
			searchCriteria.add(Restrictions.ilike("nome", filterDoacao.getNome(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nome", filterDoacao.getNome(), MatchMode.ANYWHERE));
		}

		if (filterDoacao.getEmail() != null) {
			searchCriteria.add(Restrictions.ilike("email", filterDoacao.getEmail(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("email", filterDoacao.getEmail(), MatchMode.ANYWHERE));
		}

		if (filterDoacao.getTelefone() != null) {
			searchCriteria.add(Restrictions.ilike("telefone", filterDoacao.getTelefone(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("telefone", filterDoacao.getTelefone(), MatchMode.ANYWHERE));
		}

		return new Paginator<Doacao>(searchCriteria, countCriteria).paginate(paginationParams);
	}

	public List<Doacao> filter(PaginationParams paginationParams) {
		List<Doacao> list = new ArrayList<Doacao>();
		FilterDoacao filterDoacao = (FilterDoacao) paginationParams.getFilter();

		Criteria searchCriteria = criteria();
		
		if (filterDoacao.getTitulo() != null) {
			searchCriteria.add(Restrictions.eq("titulo", filterDoacao.getTitulo()));
		}

		if (filterDoacao.getDescricao() != null) {
			searchCriteria.add(Restrictions.eq("descricao", filterDoacao.getDescricao()));
		}
		
		if (filterDoacao.getCategoria() != null) {
			searchCriteria.add(Restrictions.eq("categoria", filterDoacao.getCategoria()));
		}

		if (filterDoacao.getTipo() != null) {
			searchCriteria.add(Restrictions.eq("tipo", filterDoacao.getTipo()));
		}

		if (filterDoacao.getUf() != null) {
			searchCriteria.add(Restrictions.eq("uf", filterDoacao.getUf()));
		}

		if (filterDoacao.getCidade() != null) {
			searchCriteria.add(Restrictions.eq("cidade", filterDoacao.getCidade()));
		}

		if (filterDoacao.getBairro() != null) {
			searchCriteria.add(Restrictions.eq("bairro", filterDoacao.getBairro()));
		}

		if (filterDoacao.getNome() != null) {
			searchCriteria.add(Restrictions.eq("nome", filterDoacao.getNome()));
		}

		if (filterDoacao.getEmail() != null) {
			searchCriteria.add(Restrictions.eq("email", filterDoacao.getEmail()));
		}

		if (filterDoacao.getTelefone() != null) {
			searchCriteria.add(Restrictions.eq("telefone", filterDoacao.getTelefone()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
	
	public List<Doacao> mapaDoacao(Boolean last, String titulo){
		List<Doacao> mapaDoacao = new ArrayList<Doacao>();
		StringBuilder sb = new StringBuilder();
		String constraints = null;
		sb.append("1 = 1");
		
		if(last)
			sb.append(" and tempo_ultima_posicao >= dateadd(minute, -15 ,getdate()) ");
		
		if(!titulo.isEmpty())
			sb.append(" and titulo ilike '%" + titulo + "%' ");

		constraints = sb.toString();
		
		if(constraints.equals("1 = 1"))
			constraints = "1 = 2";
		
		SQLQuery query = nativeQuery("SELECT id, titulo, categoria, latitude, longitude FROM doacao WHERE " + constraints);
		query.setReadOnly(true);
		query.setResultTransformer(Transformers.aliasToBean(Doacao.class));
		mapaDoacao.addAll(query.list());
		return mapaDoacao;
	}
}
