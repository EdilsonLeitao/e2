package br.com.e2.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.e2.core.persistence.AccessibleHibernateDao;
import br.com.e2.core.persistence.pagination.Pagination;
import br.com.e2.core.persistence.pagination.PaginationParams;
import br.com.e2.core.persistence.pagination.Paginator;
import br.com.e2.model.Usuario;
import br.com.e2.model.filter.FilterUsuario;

@Named
@SuppressWarnings("rawtypes")
public class DaoUsuario extends AccessibleHibernateDao<Usuario> {
	private static final Logger LOGGER = Logger.getLogger(DaoUsuario.class);

	public DaoUsuario() {
		super(Usuario.class);
	}
	
	public Usuario findByUsername(String username) {
		Usuario usuario = null;
		try {
			usuario = (Usuario) criteria().add(Restrictions.eq("username", username)).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("Erro ao obter Usuário pelo username," + username, e);
		}
		return usuario;
	}

	@Override
	public Pagination<Usuario> getAll(PaginationParams paginationParams) {
		
		FilterUsuario filterUsuario = (FilterUsuario) paginationParams.getFilter();
		
		Criteria searchCriteria = criteria();
		
		Criteria countCriteria = criteria();
		
		if (filterUsuario.getNome() != null) {
			searchCriteria.add(Restrictions.ilike("nome", filterUsuario.getNome(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("nome", filterUsuario.getNome(), MatchMode.ANYWHERE));
		}
		
		if (filterUsuario.getUsername() != null) {
			searchCriteria.add(Restrictions.ilike("username", filterUsuario.getUsername(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("username", filterUsuario.getUsername(), MatchMode.ANYWHERE));
		}
		
		if (filterUsuario.getPassword() != null) {
			searchCriteria.add(Restrictions.ilike("password", filterUsuario.getPassword(), MatchMode.ANYWHERE));
			countCriteria.add(Restrictions.ilike("password", filterUsuario.getPassword(), MatchMode.ANYWHERE));
		}

		return new Paginator<Usuario>(searchCriteria, countCriteria).paginate(paginationParams);
	}
	
	public List<Usuario> filter(PaginationParams paginationParams) {
		
		List<Usuario> list = new ArrayList<Usuario>();
		
		FilterUsuario filterUsuario = (FilterUsuario) paginationParams.getFilter();
		
		Criteria searchCriteria = criteria();
		
		if (filterUsuario.getNome() != null) {
			searchCriteria.add(Restrictions.eq("nome", filterUsuario.getNome()));
		}
		
		if (filterUsuario.getUsername() != null) {
			searchCriteria.add(Restrictions.eq("username", filterUsuario.getUsername()));
		}
		
		if (filterUsuario.getPassword() != null) {
			searchCriteria.add(Restrictions.eq("password", filterUsuario.getPassword()));
		}

		if (filterUsuario.getImage() != null) {
			searchCriteria.add(Restrictions.eq("image", filterUsuario.getImage()));
		}

		list.addAll(searchCriteria.list());
		return list;
	}
}
