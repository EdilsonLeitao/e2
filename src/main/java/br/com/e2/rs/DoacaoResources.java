package br.com.e2.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.e2.core.json.JsonError;
import br.com.e2.core.json.JsonPaginator;
import br.com.e2.core.persistence.pagination.Pager;
import br.com.e2.core.persistence.pagination.PaginationParams;
import br.com.e2.core.rs.exception.ValidationException;
import br.com.e2.core.utils.Parser;
import br.com.e2.json.JsonDoacao;
import br.com.e2.model.Doacao;
import br.com.e2.model.filter.FilterDoacao;
import br.com.e2.service.DoacaoService;

@Path("/crud/doacao")
public class DoacaoResources {

	@Inject
	DoacaoService doacaoService;
	
	public static final Logger LOGGER = Logger.getLogger(DoacaoResources.class);

	@GET
	@Path("mapaDoacoes")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response mapaDoacoes(@QueryParam("last") Boolean last, @QueryParam("titulo") String titulo) {
		Response response = null;
		try {
			List<JsonDoacao> jsonDoacao = Parser.toListJsonDoacao(doacaoService.mapaDoacao(last, titulo));
			response = Response.ok(jsonDoacao).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}
	
	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterDoacao> paginationParams = new PaginationParams<FilterDoacao>(uriInfo, FilterDoacao.class);

			List<JsonDoacao> jsonDoacao = Parser.toListJsonDoacao(doacaoService.filter(paginationParams));
			response = Response.ok(jsonDoacao).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonDoacao> jsonDoacaos = Parser.toListJsonDoacao(doacaoService.all());
			response = Response.ok(jsonDoacaos).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Doacao> doacaos = null;

		try {
			PaginationParams<FilterDoacao> paginationParams = new PaginationParams<FilterDoacao>(uriInfo, FilterDoacao.class);
			doacaos = doacaoService.all(paginationParams);
			JsonPaginator<JsonDoacao> paginator = new JsonPaginator<JsonDoacao>(Parser.toListJsonDoacao(doacaos.getItens()), doacaos.getActualPage(), doacaos.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar doacaos para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Doacao doacao = doacaoService.get(id);

			return Response.ok().entity(Parser.toJson(doacao)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonDoacao jsonDoacao) {
		try {

			Doacao doacao = Parser.toEntity(jsonDoacao);
			doacao = doacaoService.save(doacao);
			return Response.ok().entity(Parser.toJson(doacao)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonDoacao.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonDoacao, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  doacao [ %s ] parametros [ %s ]", e.getMessage(), jsonDoacao.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonDoacao)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonDoacao jsonDoacao) {
		try {
			Doacao doacao = Parser.toEntity(jsonDoacao);

			doacao = doacaoService.save(doacao);
			return Response.ok().entity(Parser.toJson(doacao)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonDoacao.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonDoacao, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonDoacao.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonDoacao)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(doacaoService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
