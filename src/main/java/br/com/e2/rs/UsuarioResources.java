package br.com.e2.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.e2.core.json.JsonError;
import br.com.e2.core.json.JsonPaginator;
import br.com.e2.core.persistence.pagination.Pager;
import br.com.e2.core.persistence.pagination.PaginationParams;
import br.com.e2.core.rs.exception.ValidationException;
import br.com.e2.core.utils.Parser;
import br.com.e2.json.JsonUsuario;
import br.com.e2.model.Usuario;
import br.com.e2.model.filter.FilterUsuario;
import br.com.e2.service.UsuarioService;

@Path("/crud/usuario")
public class UsuarioResources {

	@Inject
	UsuarioService usuarioService;

	//@Inject
	//SpringSecurityUserContext context;

	public static final Logger LOGGER = Logger.getLogger(UsuarioResources.class);

	@GET
	@Path("filter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterUsuario> paginationParams = new PaginationParams<FilterUsuario>(uriInfo, FilterUsuario.class);

			List<JsonUsuario> jsonUsuario = Parser.toListJsonUsuarios(usuarioService.filter(paginationParams));
			response = Response.ok(jsonUsuario).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all() {
		Response response = null;
		try {
			List<JsonUsuario> jsonUsuarios = Parser.toListJsonUsuarios(usuarioService.all());
			response = Response.ok(jsonUsuarios).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Usuario> usuario = null;

		try {
			PaginationParams<FilterUsuario> paginationParams = new PaginationParams<FilterUsuario>(uriInfo, FilterUsuario.class);
			usuario = usuarioService.all(paginationParams);
			JsonPaginator<JsonUsuario> paginator = new JsonPaginator<JsonUsuario>(Parser.toListJsonUsuarios(usuario.getItens()), usuario.getActualPage(), usuario.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar s para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Usuario usuario = usuarioService.get(id);

			return Response.ok().entity(Parser.toJson(usuario)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(JsonUsuario jsonUsuario) {
		try {

			Usuario usuario = Parser.toEntity(jsonUsuario);
			usuario = usuarioService.save(usuario);
			return Response.ok().entity(Parser.toJson(usuario)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonUsuario.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonUsuario, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  user [ %s ] parametros [ %s ]", e.getMessage(), jsonUsuario.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonUsuario)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") Integer id, JsonUsuario jsonUsuario) {
		try {
			Usuario usuario = Parser.toEntity(jsonUsuario);

			usuario = usuarioService.update(usuario);
			return Response.ok().entity(Parser.toJson(usuario)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonUsuario.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonUsuario, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonUsuario.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonUsuario)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(usuarioService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@GET
	@Path("current")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response current() {
		Response response = null;
		try {

			//response = Response.ok(Parser.toJson(context.getCurrentUser())).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("changePassword")
	public Response update(JsonUsuario jsonUsuario) {
		try {
			Usuario usuario = Parser.toEntity(jsonUsuario);

			usuario = usuarioService.changePassword(usuario, usuario.getPassword());

			return Response.ok().entity(Parser.toJson(usuario)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonUsuario.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonUsuario, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonUsuario.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonUsuario)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("changeUser/{id}")
	public Response updateUSer(@PathParam("id") Integer id, JsonUsuario jsonUsuario) {
		try {

			//Usuario currentUser = context.getCurrentUser();
			//currentUser.setNome(jsonUsuario.getNome());
			//currentUser.setImage(jsonUsuario.getImage());

			//usuarioService.update(currentUser);

			return Response.ok().entity(Boolean.TRUE).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonUsuario.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonUsuario, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonUsuario.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonUsuario)).build();
		}
	}
}
