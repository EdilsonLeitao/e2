package br.com.e2.service;

import java.util.List;

import org.joda.time.LocalDateTime;

import br.com.e2.core.persistence.pagination.Pager;
import br.com.e2.core.persistence.pagination.PaginationParams;
import br.com.e2.model.Doacao;

public interface DoacaoService {

	Doacao get(Integer id);

	List<Doacao> all();
	
	List<Doacao> mapaDoacao(Boolean last, String titulo);
	
	Pager<Doacao> all(PaginationParams paginationParams);
	
	List<Doacao> filter(PaginationParams paginationParams);
	
	List<Doacao> search(String searchText);

	Doacao save(Doacao entity);

	Doacao update(Doacao entity);

	List<Doacao> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);
}
