package br.com.e2.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.e2.core.persistence.pagination.Pager;
import br.com.e2.core.persistence.pagination.Pagination;
import br.com.e2.core.persistence.pagination.PaginationParams;
import br.com.e2.model.Doacao;
import br.com.e2.persistence.DaoDoacao;

@Named
@Transactional
public class DoacaoServiceImp implements DoacaoService {

	private static final Logger LOGGER = Logger.getLogger(DoacaoServiceImp.class);
	
	@Inject
	DaoDoacao daoDoacao;

	@Override
	public Doacao get(Integer id) {
		return daoDoacao.find(id);
	}

	@Override
	public Pager<Doacao> all(PaginationParams paginationParams) {
		Pagination<Doacao> pagination = daoDoacao.getAll(paginationParams);
		return new Pager<Doacao>(pagination.getResults(), 0, pagination.getTotalRecords());
	}
	
	@Override
	public List<Doacao> filter(PaginationParams paginationParams) {
		List<Doacao> list = daoDoacao.filter(paginationParams);
		return list;
	}
	
	@Override
	public List<Doacao> all() {
		return daoDoacao.getAll();
	}
	
	@Override
	public List<Doacao> search(String description) {
		return new ArrayList<Doacao>();
	}
	
	public List<Doacao> last(LocalDateTime lastSyncDate){
		return daoDoacao.last(lastSyncDate);
	}
			
	@Override
	public Doacao save(Doacao entity) {
		return daoDoacao.save(entity);
	}

	@Override
	public Doacao update(Doacao entity) {
		return daoDoacao.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoDoacao.delete(id);
	}
	
	@Override
	public List<Doacao> mapaDoacao(Boolean last, String titulo) {
		return daoDoacao.mapaDoacao(last, titulo);
	}
	
}
