package br.com.e2.service;

import java.util.List;

import org.joda.time.LocalDateTime;

import br.com.e2.core.persistence.pagination.Pager;
import br.com.e2.core.persistence.pagination.PaginationParams;
import br.com.e2.model.Usuario;

public interface UsuarioService {

	Usuario get(Integer id);

	List<Usuario> all();
	
	Pager<Usuario> all(PaginationParams paginationParams);

	List<Usuario> filter(PaginationParams paginationParams);
	
	List<Usuario> search(String searchText);

	Usuario save(Usuario entity);

	Usuario update(Usuario entity);
    
	List<Usuario> last(LocalDateTime lastSyncDate);		

	Boolean delete(Integer id);

	Usuario changePassword(Usuario currentUser, String password);
}
