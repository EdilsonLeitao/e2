package br.com.e2.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.e2.core.persistence.pagination.Pager;
import br.com.e2.core.persistence.pagination.Pagination;
import br.com.e2.core.persistence.pagination.PaginationParams;
import br.com.e2.model.Usuario;
import br.com.e2.persistence.DaoUsuario;

@Named
@Transactional
public class UsuarioServiceImp implements UsuarioService {

	private static final Logger LOGGER = Logger.getLogger(UsuarioServiceImp.class);
	
	@Inject
	DaoUsuario daoUsuario;

	@Override
	public Usuario get(Integer id) {
		return daoUsuario.find(id);
	}

	@Override
	public Pager<Usuario> all(PaginationParams paginationParams) {
		Pagination<Usuario> pagination = daoUsuario.getAll(paginationParams);
		return new Pager<Usuario>(pagination.getResults(), 0, pagination.getTotalRecords());
	}

	@Override
	public List<Usuario> filter(PaginationParams paginationParams) {
		List<Usuario> list = daoUsuario.filter(paginationParams);
		return list;
	}

	@Override
	public List<Usuario> all() {
		return daoUsuario.getAll();
	}

	@Override
	public List<Usuario> search(String description) {
		return new ArrayList<Usuario>();
	}

	public List<Usuario> last(LocalDateTime lastSyncDate) {
		return daoUsuario.last(lastSyncDate);
	}

	@Override
	public Usuario save(Usuario entity) {

		//BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		//String hashedPassword = passwordEncoder.encode(entity.getPassword());
		//entity.setPassword(hashedPassword);

		return daoUsuario.save(entity);
	}

	@Override
	public Usuario update(Usuario entity) {
		return daoUsuario.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		return daoUsuario.delete(id);
	}

	@Override
	public Usuario changePassword(Usuario currentUser, String password) {
		//BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		//String hashedPassword = passwordEncoder.encode(password);

		//currentUser.setPassword(hashedPassword);

		return update(currentUser);
	}

}
