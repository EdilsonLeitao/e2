<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Projeto de Estágio - UNI7">
		<meta name="author" content="ThemeBucket">
		<link rel="icon" href="images/logo.png" type="image/png">
		<title>Projeto E2</title>
		<link rel="stylesheet" href="css/all.css" />
	</head>
	
	<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDl_wqY1zyVPExj6ZLVaY6D4blvWUR52zU&libraries=geometry,places"></script>
		
	<body class="no-skin">
        <div class="wrapper">


	        <div id="content">
				<div class="main-container" id="main-container">
					<div class="main-content">
						<div class="main-content-inner">
							<div class="page-content">
								<div class="main-principal">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script data-main="js/main" src="vendor/require/require.js"></script>
	</body>
</html>