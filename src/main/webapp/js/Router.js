define(function(require) {
	var $ = require('adapters/jquery-adapter');
	var _ = require('adapters/underscore-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');
	// var cliente = require('adapters/auth-adapter');

	// var PageUserProfile = require('views/user/PageUserProfile');
	// var PageUser = require('views/user/PageUser');
	// var FormUsuario = require('views/usuario/FormUsuario');
	// var UserModel = require('models/UserModel');

	var PageDoacao = require('views/doacao/PageDoacao');
	var FormDoacao = require('views/doacao/FormDoacao');
	var PageMapaDoacao = require('views/doacao/PageMapaDoacao');
	var DoacaoModel = require('models/DoacaoModel');

	util.NProgress.setBlockerPanel('block_panel');

	var CustomRegion = Marionette.Region.extend({
		el : ".main-content",

		attachHtml : function(view) {
			this.$el.hide();
			this.$el.html(view.el);
			util.scrollTop();
			this.$el.fadeIn(300);

			view.listenTo(view, 'show', function() {
				setTimeout(function() {
					util.NProgress.done(false, true);
				}, 100);
			});
		},
	});

	var AppRouter = Backbone.Router.extend({
		routes : {
			// '' : 'index',
			// '' : 'userProfile',
			'' : 'mapaDoacao',
			'#' : 'mapaDoacao',

			// hashs de User
			'app/userProfile' : 'userProfile',
			'app/users' : 'users',
			'app/novoUsuario' : 'novoUsuario',
			'app/editUser/:id' : 'editUser',

			// hashs de Doacao
			'app/doacao' : 'doacao',
			'app/newDoacao' : 'newDoacao',
			'app/mapaDoacao' : 'mapaDoacao',
			'app/editDoacao/:id' : 'editDoacao',

		},

		initialize : function() {
			this.App = new Marionette.Application();
			this.App.addRegions({
				mainRegion : CustomRegion
			});
			this.on('route', function(abc) {
				util.NProgress.start(true);
			});
		},

		// configuração das rotas de User
		// users : function() {
		// util.markActiveItem('users');
		// this.pageUser = new PageUser();
		// this.App.mainRegion.show(this.pageUser);
		// util.breadcrumb({
		// iconClass : 'fa-desktop',
		// itemLabel : 'Usuário',
		// itemSubFolderName : 'Listagem',
		// url : 'app/users'
		// });
		// },
		//
		// userProfile : function() {
		// var that = this;
		//
		// util.markActiveItem('userProfile');
		// var userProfileModel = new UserModel();
		//
		// userProfileModel.getCurrentUser({
		// success : function(model) {
		// that.pageUserProfile = new PageUserProfile({
		// model : userProfileModel,
		// });
		// that.App.mainRegion.show(that.pageUserProfile);
		// },
		// error : function(x, y, z) {
		// console.error(x, y, z);
		// }
		// });
		//
		// this.pageUserProfile = new PageUserProfile({
		// model : userProfileModel,
		// });
		// this.App.mainRegion.show(this.pageUserProfile);
		// util.breadcrumb({
		// iconClass : 'fa-desktop',
		// itemLabel : 'Usuário',
		// itemSubFolderName : 'Perfil do usuário',
		// url : '#'
		// });
		// },
		//
		// novoUsuario : function() {
		// util.markActiveItem('users');
		// var formUsuario = new FormUsuario({
		// model : new UserModel(),
		// });
		// this.App.mainRegion.show(formUsuario);
		// util.breadcrumb({
		// iconClass : 'fa-desktop',
		// itemLabel : 'Usuário',
		// itemSubFolderName : 'Formulário de cadastro de Usuário',
		// url : 'app/novoUsuario'
		// });
		// },
		//
		// editUser : function(idUser) {
		// var that = this;
		// util.markActiveItem('users');
		// var formUsuario = null;
		// if (this.pageUser) {
		// formUsuario = new FormUsuario({
		// model : this.pageUser.users.get(idUser),
		// });
		// that.App.mainRegion.show(formUsuario);
		// } else {
		// var model = new UserModel({
		// id : idUser,
		// })
		// model.fetch({
		// success : function(model) {
		// formUsuario = new FormUsuario({
		// model : model,
		// });
		// that.App.mainRegion.show(formUsuario);
		// },
		// error : function(x, y, z) {
		// console.error(x, y, z);
		// }
		// })
		// util.breadcrumb({
		// iconClass : 'fa-calendar',
		// itemLabel : 'Usuário',
		// itemSubFolderName : 'Formulário de atualização de Usuário',
		// url : 'app/users'
		// });
		// }
		// },

		// configuração das rotas de Doacao
		doacao : function() {
			util.markActiveItem('doacao');
			this.pageDoacao = new PageDoacao();
			this.App.mainRegion.show(this.pageDoacao);
			util.breadcrumb({
				iconClass : 'fa-hand-paper-o',
				itemLabel : 'Doações',
				itemSubFolderName : 'Lista de Doações',
				url : 'app/doacao'
			});
		},

		newDoacao : function() {
			util.markActiveItem('doacao');
			var formDoacao = new FormDoacao({
				model : new DoacaoModel(),
			});
			this.App.mainRegion.show(formDoacao);
			util.breadcrumb({
				iconClass : 'fa-hand-paper-o',
				itemLabel : 'Doação',
				itemSubFolderName : 'Formulário de cadastro de Doação',
				url : 'app/doacao'
			});
		},

		editDoacao : function(idDoacao) {
			var that = this;
			util.markActiveItem('doacao');
			var formDoacao = null;
			if (this.pageDoacao) {
				formDoacao = new FormDoacao({
					model : this.pageDoacao.doacao.get(idDoacao),
				});
				that.App.mainRegion.show(formDoacao);
			} else {
				var model = new DoacaoModel({
					id : idDoacao,
				})
				model.fetch({
					success : function(model) {
						formDoacao = new FormDoacao({
							model : model,
						});
						that.App.mainRegion.show(formDoacao);
					},
					error : function(x, y, z) {
						console.error(x, y, z);
					}
				})
				util.breadcrumb({
					iconClass : 'fa-hand-paper-o',
					itemLabel : 'Doação',
					itemSubFolderName : 'Formulário de atualização de Doação',
					url : 'app/doacao'
				});
			}
		},

		mapaDoacao : function() {
			util.markActiveItem('doacao');
			this.pageMapaDoacao = new PageMapaDoacao();
			this.App.mainRegion.show(this.pageMapaDoacao);
			util.breadcrumb({
				iconClass : 'fa-map',
				itemLabel : 'Doação',
				itemSubFolderName : 'Mapa de Doações',
				url : 'app/doacao'
			});
		},

		start : function() {
			Backbone.history.start();
		}
	});
	return AppRouter;
});
