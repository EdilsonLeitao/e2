define(function(require) {

	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseCollection = require('collections/BaseCollection');
	var DoacaoModel = require('models/DoacaoModel');
	var BaseCollection = require('collections/BaseCollection');	

	var DoacaoCollection = BaseCollection.extend({
		model : DoacaoModel,
		
		url : 'rs/crud/doacao/all',
		
		getMapaDoacoes : function(options) {
			this.url = 'rs/crud/doacao/mapaDoacoes';
			this.fetch(options);
			
		},
	});
	
	return DoacaoCollection;
});