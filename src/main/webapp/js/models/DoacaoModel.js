define(function(require) {

	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var BaseModel = require('models/BaseModel');

	var DoacaoModel = BaseModel.extend({

		urlRoot : 'rs/crud/doacao',

		defaults : {
			id: null,
			titulo: '',
			descricao: '',
			categoria: '',
			tipo: '',
			img1: '',
			img2: '',
			img3: '',
			img4: '',
		    	endereco : '',    	
		    	numeroEndereco : '',    	
		    	complemento : '',    	
		    	uf : '',    	
		    	cidade : '',    	
		    	bairro : '',    	
		    	cep : '',    	
		    	referenciaEndereco : '',    	
		    	latitude : null,
		    	longitude : null,
		    	nome: '',
		    	email: '',
		    	telefone: '',
		}
	});
	return DoacaoModel;
});
