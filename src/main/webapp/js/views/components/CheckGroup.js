define([ 'utilities/utils', 'adapters/underscore-adapter', 'adapters/jquery-adapter', 'adapters/backbone-adapter', 'marionette' ],//

function(util, _, $, Backbone, Marionette) {
	CheckGroup = Backbone.View.extend({
		initialize : function(options) {
			this.container = options.container;
			this.checkboxes = this.container.find("input[type=checkbox]")
		},

		getValue : function() {
			var that = this;
			var checkValues = [];
			_.each(that.checkboxes, function(checkbox) {
				var $checkbox = $(checkbox);
				if ($checkbox.is(':checked')) {
					checkValues.push($checkbox.val());
				}
			})
			return checkValues.join(',');
		},

		setValue : function(values) {

			var models = ('' + values).split(',');
			var that = this;
			that.clear();
			_.each(models, function(model) {
				_.each(that.checkboxes, function(checkbox) {
					var $checkbox = $(checkbox);
					if ($checkbox.val() == model) {
						$checkbox.prop('checked', true);
					}
				});
			});
		},
		clear : function() {
			_.each(this.checkboxes, function(checkbox) {
				var $checkbox = $(checkbox);
				$checkbox.prop('checked', false);
			})
		}
	});
	return CheckGroup;
});