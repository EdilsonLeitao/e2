define(function(require) {

	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var util = require('utilities/utils');

	var GMap = Marionette.ItemView.extend({

		initialize : function(options) {
			this.directionsService = new google.maps.DirectionsService;

			this.directionsDisplay = new google.maps.DirectionsRenderer;

			this.showDrawTools = options.showDrawTools;

			if (!options.mapElement) {
				throw new TypeError("Voce deve indicar qual o container do map.");
			}

			this.criaMapa(options)
		},

		criaMapa : function(options) {
			this.overlays = [];
			this.markers = [];

			this.markersMap = new Col.Map();

			var that = this;

			this.googlemap = new google.maps.Map(options.mapElement[0], {
				center : (options.mapOptions && options.mapOptions.center) || new google.maps.LatLng(-3.7710272, -38.48364950000001),
				zoom : 14,//(options.mapOptions && options.mapOptions.zoom) || 14,
				mapTypeId : (options.mapOptions && options.mapOptions.mapTypeId) || google.maps.MapTypeId.ROADMAP,
				mapTypeControl: false,
				//Nigth
				/*styles : 
					[ { "elementType": "geometry", "stylers": [ { "color": "#242f3e" } ] }, { "elementType": "labels.text.fill", "stylers": [ { "color": "#746855" } ] }, { "elementType":
						"labels.text.stroke", "stylers": [ { "color": "#242f3e" } ] }, { "featureType": "administrative.locality", "elementType": "labels.text.fill", "stylers": [ { "color":
						"#d59563" } ] }, { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [ { "color": "#d59563" } ] }, { "featureType": "poi.park", "elementType":
						"geometry", "stylers": [ { "color": "#263c3f" } ] }, { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [ { "color": "#6b9a76" } ] }, {
						"featureType": "road", "elementType": "geometry", "stylers": [ { "color": "#38414e" } ] }, { "featureType": "road", "elementType": "geometry.stroke", "stylers": [ {
						"color": "#212a37" } ] }, { "featureType": "road", "elementType": "labels.text.fill", "stylers": [ { "color": "#9ca5b3" } ] }, { "featureType": "road.highway",
						"elementType": "geometry", "stylers": [ { "color": "#746855" } ] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "color":
						"#1f2835" } ] }, { "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [ { "color": "#f3d19c" } ] }, { "featureType": "transit",
						"elementType": "geometry", "stylers": [ { "color": "#2f3948" } ] }, { "featureType": "transit.station", "elementType": "labels.text.fill", "stylers": [ { "color":
						"#d59563" } ] }, { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#17263c" } ] }, { "featureType": "water", "elementType":
						"labels.text.fill", "stylers": [ { "color": "#515c6d" } ] }, { "featureType": "water", "elementType": "labels.text.stroke", "stylers": [ { "color": "#17263c" } ] } 
					]*/
				//Retro
				styles : 
					[ { "elementType": "geometry", "stylers": [  { "color": "#ebe3cd" } ] }, { "elementType": "labels.text.fill", "stylers": [ {  "color": "#523735" } ] }, { "elementType":
						"labels.text.stroke", "stylers": [ { "color": "#f5f1e6" } ] }, {  "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [ { "color": "#c9b2a6" }
						]   },  {   "featureType":  "administrative.land_parcel",   "elementType":  "geometry.stroke",   "stylers":   [  {   "color":  "#dcd2be"   }  ]   },  {   "featureType":
						"administrative.land_parcel",  "elementType": "labels.text.fill",  "stylers":  [  { "color":  "#ae9e90"  } ]  },  {  "featureType": "landscape.natural",  "elementType":
						"geometry", "stylers":  [ { "color":  "#dfd2ae" } ] },  { "featureType": "poi",  "elementType": "geometry", "stylers":  [ { "color": "#dfd2ae"  } ] },  { "featureType":
						"poi", "elementType": "labels.text.fill", "stylers": [ { "color": "#93817c" } ]  }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [ { "color":
						"#a5b076" }  ] }, {  "featureType": "poi.park", "elementType":  "labels.text.fill", "stylers":  [ { "color":  "#447530" } ]  }, { "featureType":  "road", "elementType":
						"geometry",  "stylers": [  { "color":  "#f5f1e6" }  ] },  { "featureType":  "road.arterial",  "elementType": "geometry",  "stylers": [  { "color":  "#fdfcf8" }  ] },  {
						"featureType": "road.highway", "elementType":  "geometry", "stylers": [ { "color":  "#f8c967" } ] }, { "featureType":  "road.highway", "elementType": "geometry.stroke",
						"stylers": [  { "color": "#e9bc62"  } ] },  { "featureType": "road.highway.controlled_access",  "elementType": "geometry", "stylers":  [ { "color":  "#e98d58" } ]  }, {
						"featureType":  "road.highway.controlled_access",  "elementType":  "geometry.stroke",  "stylers":  [  {  "color":  "#db8555"  }  ]  },  {  "featureType":  "road.local",
						"elementType":  "labels.text.fill", "stylers":  [ {  "color": "#806b63"  } ]  }, {  "featureType": "transit.line",  "elementType": "geometry",  "stylers": [  { "color":
						"#dfd2ae"  } ]  }, {  "featureType": "transit.line",  "elementType": "labels.text.fill",  "stylers": [  { "color":  "#8f7d77" }  ] },  { "featureType":  "transit.line",
						"elementType": "labels.text.stroke", "stylers":  [ { "color": "#ebe3cd"  } ] }, { "featureType":  "transit.station", "elementType": "geometry", "stylers":  [ { "color":
						"#dfd2ae"  } ]  }, {  "featureType":  "water", "elementType":  "geometry.fill", "stylers":  [  { "color":  "#b9d3c2" }  ]  }, {  "featureType": "water",  "elementType":
						"labels.text.fill", "stylers": [ { "color": "#92998d" } ] 
					} ]

			});

			if (this.showDrawTools) {
				this.drawingManager = new google.maps.drawing.DrawingManager({
					drawingControl : true,
					drawingControlOptions : {

						style : google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
						position : google.maps.ControlPosition.TOP_CENTER,
						drawingModes : [ google.maps.drawing.OverlayType.POLYGON ]
					},
					polygonOptions : {
						fillColor : '#ddff00',
						fillOpacity : 0.5,
						strokeWeight : 3,
						clickable : true,
						editable : true,
						zIndex : 1
					},

				});

				google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function(event) {
					if (event.type == google.maps.drawing.OverlayType.CIRCLE) {
						var radius = event.overlay.getRadius();
					}
					google.maps.event.addListener(event.overlay, 'rightclick', that.mostraAlgumaCoisa);
					that.overlays.push(event.overlay);
				});
				this.drawingManager.setMap(this.googlemap);
			}
		},

		desenhaRota : function(arrayLatlongs) {
			var that = this;
			var dirOpts = {}
			var len = arrayLatlongs.length;
			if (len < 2) {
				that.directionsDisplay.setMap(null);
				return false;
			}
			if (len == 2) {
				dirOpts = {
					origin : arrayLatlongs[0],
					destination : arrayLatlongs[len - 1],
					travelMode : google.maps.TravelMode.DRIVING
				}
			}
			if (len > 2) {
				var waypoints = []
				_.each(arrayLatlongs.slice(1, len - 1), function(ponto) {
					waypoints.push({
						location : ponto,
						stopover : true
					})
				});

				dirOpts = {
					origin : arrayLatlongs[0],
					destination : arrayLatlongs[len - 1],
					waypoints : waypoints,
					optimizeWaypoints : true,
					travelMode : google.maps.TravelMode.DRIVING
				}
			}

			this.directionsService.route(dirOpts, function(response, status) {
				if (status === google.maps.DirectionsStatus.OK) {
					that.directionsDisplay.setMap(that.googlemap);
					that.directionsDisplay.setDirections(response);
					that.googlemap.setZoom(11);

				} else {
					console.warn('Directions request failed due to ' + status);
				}
			});

		},
		
		mostraAlgumaCoisa : function(evt) {
			console.log(evt)
		},

		getOverlays : function() {
			return this.overlays;
		},

		addMarker : function(jsonLatLng, identify) {
			var point = new google.maps.LatLng(jsonLatLng.lat, jsonLatLng.lng);
			var marker = new google.maps.Marker({
				position : point,
				map : this.googlemap,
				title : identify
			});

			this.markers.push(marker);
			this.markersMap.put(identify, marker);
			this.googlemap.setCenter(point);
			this.googlemap.setZoom(14);
		},

		addDoacoes : function(collection) {
			var that = this;

//	        	'<div id="content">'+
//	        		'<div id="siteNotice">'+
//	        		'</div>'+
//	        		
//	        		'<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
//	        			'<div id="bodyContent">'+
//	        				'<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
//	        					'sandstone rock formation in the southern part of the '+
//	        					'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
//	        					'south west of the nearest large town, Alice Springs; 450&#160;km '+
//	        					'(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
//	        					'features of the Uluru - Kata Tjuta National Park. Uluru is '+
//	        					'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
//	        					'Aboriginal people of the area. It has many springs, waterholes, '+
//	        					'rock caves and ancient paintings. Uluru is listed as a World '+
//	        					'Heritage Site.'+
//	        				'</p>'+
//	        				'<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
//	        					'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
//	        					'(last visited June 22, 2009).'+ 
//	        				'</p>'+
//	        			'</div>'+
//            '</div>';
	        
			_.each(collection, function(element) {
				var point = new google.maps.LatLng(element.latitude, element.longitude);
		        var contentString = '<p>Título: '+ element.titulo+'</p>'+
		        		'<p>Tipo: '+ element.tipo+'</p>'+
		        		'<p>Categoria: '+ util.nomeCategoria[element.categoria]+'</p>'+
					'<a class="go-back-link btn btn-light" href="#app/editDoacao/'+element.id+"\">"+
						'<i class="ace-icon bigger-110"></i>'+
						'Abrir'+
					'</a>'


				var infowindow = new google.maps.InfoWindow({
		            content: contentString
		        });
				
				var marker = new google.maps.Marker({
					position : point,
					map : that.googlemap,
					title : element.id+"",
					icon : util.marker[element.categoria],
					//label : element.id+"",
					animation: google.maps.Animation.DROP
				});
				
				marker.addListener('click', function() {
					infowindow.open(this.googlemap, marker);
					
			    });

				that.markers.push(marker);
			})
			
		},

		clearMarkers : function() {
			var markers = this.markersMap.values()
			_.each(markers, function(marker) {
				marker.setMap(null);
			})
			
		},
		
		removeMarker : function(jsonLatLng, identify) {
			var marker = this.markersMap.get(identify)
			marker.setMap(null);
		},

		addPolygon : function(array) {
			var polygon = new google.maps.Polygon({
				paths : array,
				fillColor : '#ddff00',
				fillOpacity : 0.5,
				strokeWeight : 3,
				clickable : true,
				editable : true,
				zIndex : 1
			});
			polygon.setMap(this.googlemap);

			this.overlays.push(polygon);
		},

		clear : function() {
			_.each(this.markers, function(marker) {
				marker.setMap(null)
			})
			this.directionsDisplay.setMap(null)
			this.markers = [];
			this.googlemap.setCenter({lat: -3.7710272, lng: -38.48364950000001});
			this.googlemap.setZoom(14);
		}
	});

	return GMap;
})
