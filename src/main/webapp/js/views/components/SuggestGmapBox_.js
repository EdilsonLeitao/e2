define(function(require) {
	// Start "ImportÂ´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var util = require('utilities/utils');
	var BaseCollection = require('collections/BaseCollection');

	var estados = {
		'Acre' : 'AC',
		'Alagoas' : 'AL',
		'Amapá' : 'AP',
		'Amazonas' : 'AM',
		'Bahia ' : 'BA',
		'Ceará' : 'CE',
		'Distrito Federal ' : 'DF',
		'Espírito Santo' : 'ES',
		'Goiás' : 'GO',
		'Maranhão' : 'MA',
		'Mato Grosso' : 'MT',
		'Mato Grosso do Sul' : 'MS',
		'Minas Gerais' : 'MG',
		'Pará' : 'PA',
		'Paraíba' : 'PB',
		'Paraná' : 'PR',
		'Pernambuco' : 'PE',
		'Piauí' : 'PI',
		'Rio de Janeiro' : 'RJ',
		'Rio Grande do Norte' : 'RN',
		'Rio Grande do Sul' : 'RS',
		'Rondônia' : 'RO',
		'Roraima' : 'RR',
		'Santa Catarina' : 'SC',
		'São Paulo' : 'SP',
		'Sergipe' : 'SE',
		'Tocantins' : 'TO',
	}

	var Suggestbox = Backbone.View.extend({
		getValue : function() {
			this.fieldVal;
		},

		getTexto : function() {
			if (this.fieldVal) {
				return this.fieldVal.get('endereco');
			}
			return '';
		},

		getJsonValue : function() {
			if (this.fieldVal) {
				return this.fieldVal.toJSON();
			}
			return null;
		},

		clear : function() {
			this.control.clear();
		},
		setValue : function(endereco) {

		},

		setParam : function(jsonParam) {
			this.param = jsonParam
		},

		initialize : function(options) {
			var that = this;
			this.fieldVal = null;
			this.geocoder = new google.maps.Geocoder();

			this.onSelect = options.onSelect;

			var collection = new BaseCollection();

			this.selectize = this.$el.selectize({

				valueField : 'id',
				labelField : 'endereco',
				searchField : [ 'endereco', 'cidade', 'cidade', 'uf', 'numero' ],
				persist : false,
				create : false,

				render : {
					option : function(item, escape) {
						return "<div class='title'> " + "<span class='name'>  " + item.endereco + "</span>" + "</div>"
					},
				},

				load : function(query, callback) {
					if (!query.length || query.length < 5)
						return callback();

					var address = query;
					that.geocoder.geocode({
						'address' : address,
					}, function(results, status) {
						if (status === google.maps.GeocoderStatus.OK) {
							that.control.clearOptions();
							var itens = [];
							_.each(results, function(result) {

								var jsonEndereco = util.resultGeocodeToJson(result);

								itens.push({
									id : result.place_id,
									latlong : result.geometry.location.toJSON(),
									endereco : result.formatted_address,
									enderecoFormatado : result.formatted_address,
									geocoder_info : results,
									
									latitude : result.geometry.location.lat(),
									longitude : result.geometry.location.lng(),
									
									logradouro : jsonEndereco.endereco,
									numero : jsonEndereco.numero,
									complemento : '',
									uf : estados[jsonEndereco.uf],
									cidade : jsonEndereco.cidade,
									bairro : jsonEndereco.bairro,
									cep : jsonEndereco.cep,
								})
							})
							collection.add(itens);

							callback(itens);
							// control.open();
						} else {
							console.log('Endereço ' + address + ' não localizado: razão' + status);
						}
					});
				},
				onItemAdd : function(value, $item) {
					var theModel;
					collection.each(function(model) {
						if (model.get('id') + '' === value) {
							theModel = model;
						}
					})
					that.fieldVal = theModel;

					if (theModel)
						if (that.onSelect) {
							that.onSelect(theModel.toJSON());
						}

				},

			});
			this.selectize.on('change', function() {
				console.log()
				that.$el.val(that.getTexto());
			});
			var $select = this.selectize;
			this.control = $select[0].selectize;
			// this.control.setValue(that.model.get('enderecoPartida'))

		},
	});
	return Suggestbox;
});
