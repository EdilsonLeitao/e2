define(function(require) {

	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');


	var ModalContatoTemplate = require('text!views/contato/tpl/ModalContatoTemplate.html');

	var FormContato = Marionette.LayoutView.extend({
		template : _.template(ModalContatoTemplate),

		regions : {},

		events : {
		},

		ui : {
			form : '#formContato',
			modalScreen : '.modal'
		},

		initialize : function() {
			var that = this;

			this.on('show', function() {

			});
		},

		showInPage : function() {
			this.ui.modalScreen.modal({
				show : true,
				backdrop : 'static'
			});
		}
	});

	return FormContato;
});