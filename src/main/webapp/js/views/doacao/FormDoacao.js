define(function(require) {

	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var UploadView = require('views/components/UploadView');
	var TemplateFormDoacao = require('text!views/doacao/tpl/FormDoacaoTemplate.html');
	var DoacaoModel = require('models/DoacaoModel');

	var FormDoacao = Marionette.LayoutView.extend({

		template: _.template(TemplateFormDoacao),

		regions: {
			img1Region: '.img1',
			img2Region: '.img2',
			img3Region: '.img3',
			img4Region: '.img4',
		},

		events: {
			'click 	.save': 'save',
			'click 	.saveAndContinue': 'saveAndContinue',
			'click #menu-close': 'toggleSidebarWrapper',
			'click #menu-toggle': 'toggleSidebarWrapper',
		},

		ui: {
			inputId: '#inputId',
			inputTipo: '#inputTipo',
			inputCategoria: '#inputCategoria',
			inputTitulo: '#inputTitulo',
			inputDescricao: '#inputDescricao',
			inputImg1: '#inputImg1',
			inputImg2: '#inputImg2',
			inputImg3: '#inputImg3',
			inputImg4: '#inputImg4',
			inputEnderecoCombo: '.inputEnderecoCombo',
			inputEndereco: '#inputEndereco',
			inputNumeroEndereco: '#inputNumeroEndereco',
			inputComplemento: '#inputComplemento',
			inputUf: '#inputUf',
			inputCidade: '#inputCidade',
			inputBairro: '#inputBairro',
			inputCep: '#inputCep',
			inputReferenciaEndereco: '#inputReferenciaEndereco',
			inputLatitude: '#inputLatitude',
			inputLongitude: '#inputLongitude',
			inputNome: '#inputNome',
			inputEmail: '#inputEmail',
			inputTelefone: '#inputTelefone',
			sidebarWrapper: '#sidebar-wrapper',
			form: '#formDoacao',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},

		toggleSidebarWrapper: function() {
			this.ui.sidebarWrapper.toggleClass("active");
		},
		
		initialize: function() {
			var that = this;
			
			this.uploadViewImg1 = new UploadView({
				folder: "doacao",
				readOnly: that.model.get('id') ? true : false,
				bindElement: that.ui.inputImg1,
				onSuccess: function(resp, options) {
					console.info('Upload da imagem concluido...[ ' + resp + ' ]')
				},
				onError: function(resp, options) {
					console.error('Problemas ao realizar o upload da imagem.')
				}
			});

			this.uploadViewImg2 = new UploadView({
				folder: "doacao",
				readOnly: that.model.get('id') ? true : false,
				bindElement: that.ui.inputImg2,
				onSuccess: function(resp, options) {
					console.info('Upload da imagem concluido...[ ' + resp + ' ]')
				},
				onError: function(resp, options) {
					console.error('Problemas ao realizar o upload da imagem.')
				}
			});

			this.uploadViewImg3 = new UploadView({
				folder: "doacao",
				readOnly: that.model.get('id') ? true : false,
				bindElement: that.ui.inputImg3,
				onSuccess: function(resp, options) {
					console.info('Upload da imagem concluido...[ ' + resp + ' ]')
				},
				onError: function(resp, options) {
					console.error('Problemas ao realizar o upload da imagem.')
				}
			});

			this.uploadViewImg4 = new UploadView({
				folder: "doacao",
				readOnly: that.model.get('id') ? true : false,
				bindElement: that.ui.inputImg4,
				onSuccess: function(resp, options) {
					console.info('Upload da imagem concluido...[ ' + resp + ' ]')
				},
				onError: function(resp, options) {
					console.error('Problemas ao realizar o upload da imagem.')
				}
			});

			this.on('show', function() {

				this.img1Region.show(this.uploadViewImg1);
				this.img2Region.show(this.uploadViewImg2);
				this.img3Region.show(this.uploadViewImg3);
				this.img4Region.show(this.uploadViewImg4);
				
				this.ui.inputTelefone.mask(util.PHONE_BEHAVIOR, util.PHONE_OPTIONS);

				this.ui.inputEnderecoCombo.autocomplete({
					source: function (request, response) {
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode({ 'address': request.term, 'region': 'BR' }, function (results, status) {
							response($.map(results, function (item) {
								var jsonEndereco = util.addressComponentsToJson(item.address_components, item.formatted_address);
								return {
									label: item.formatted_address,
									value: item.formatted_address,
									endereco: {
										latlong: item.geometry.location.toJSON(),
										endereco: item.formatted_address,
										enderecoFormatado: item.formatted_address,
										latitude: item.geometry.location.lat(),
										longitude: item.geometry.location.lng(),
										logradouro: jsonEndereco.endereco,
										numero: jsonEndereco.numero,
										complemento: '',
										uf: util.estados[jsonEndereco.uf],
										cidade: jsonEndereco.cidade,
										bairro: jsonEndereco.bairro,
										cep: jsonEndereco.cep,
									}

								}
							}));
						})
					},
					delay: 500,
					select: function (event, ui) {
						that.populaEndereco(ui.item.endereco);
					}
				});

				this.ui.form.validationEngine('attach', {
					promptPosition: "topLeft",
					isOverflown: false,
					validationEventTrigger: "change"
				});
				if (this.model.get('id')){
					this.justToView();
				}
			});
		},
		
		justToView: function(){
			this.uploadViewImg1.off("click");
			this.ui.inputTitulo.attr('readonly', true);
			this.ui.inputTipo.attr('readonly', true);
			this.ui.inputCategoria.attr('readonly', true);
			this.ui.inputDescricao.attr('readonly', true);
			this.ui.inputImg1.attr('readonly', true);
			this.ui.inputImg2.attr('readonly', true);
			this.ui.inputImg3.attr('readonly', true);
			this.ui.inputImg4.attr('readonly', true);
			this.ui.inputEnderecoCombo.attr('readonly', true);
			this.ui.inputNome.attr('readonly', true);
			this.ui.inputEmail.attr('readonly', true);
			this.ui.inputTelefone.attr('readonly', true);
			this.ui.saveButton.hide();
		},
		
		save: function() {
			var that = this;
			var doacao = that.getModel();
			
			if (this.isValid()) {
				
				doacao.save({}, {
					
					success: function(_model, _resp, _options) {
						
						util.showSuccessMessage('Doação salva com sucesso!');
						that.clearForm();
						util.goPage('app/mapaDoacao');
					},
					
					error: function(_model, _resp, _options) {
						
						if(_resp.status == '403'){
							util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
						} else {
							util.showErrorMessage('Problema ao salvar registro! ' + _resp.responseJSON.legalMessage, _resp.responseJSON.legalMessage);
						}
					}
				});
			} else {
				util.showMessage('error', 'Verifique campos em destaque!');
			}
		},
		
		clearForm: function() {
			util.clear('inputId');
			util.clear('inputTipo');
			util.clear('inputCategoria');
			util.clear('inputTitulo');
			util.clear('inputDescricao');
			util.clear('inputImg1');
			util.clear('inputImg2');
			util.clear('inputImg3');
			util.clear('inputImg4');
			util.clear('inputEnderecoCombo');
			util.clear('inputEndereco');
			util.clear('inputNumeroEndereco');
			util.clear('inputComplemento');
			util.clear('inputUf');
			util.clear('inputCidade');
			util.clear('inputBairro');
			util.clear('inputCep');
			util.clear('inputReferenciaEndereco');
			util.clear('inputLatitude');
			util.clear('inputLongitude');
			util.clear('inputNome');
			util.clear('inputEmail');
			util.clear('inputTelefone');
		},
		
		isValid: function() {
			return this.ui.form.validationEngine('validate', {
				promptPosition: "topLeft",
				isOverflown: false,
				validationEventTrigger: "change"
			});
		},
		
		getModel: function() {
			var doacao = this.model;
			
			doacao.set({
				id: util.escapeById('inputId') || null,
				tipo: util.escapeById('inputTipo'),
				categoria: util.escapeById('inputCategoria'),
				titulo: util.escapeById('inputTitulo'),
				descricao: util.escapeById('inputDescricao'),
				img1: util.escapeById('inputImg1'),
				img2: util.escapeById('inputImg2'),
				img3: util.escapeById('inputImg3'),
				img4: util.escapeById('inputImg4'),
			    	endereco : util.escapeById('inputEndereco'),    	
			    	numeroEndereco : util.escapeById('inputNumeroEndereco'),
			    	complemento : util.escapeById('inputComplemento'),
			    	uf : util.escapeById('inputUf'),
			    	cidade : util.escapeById('inputCidade'),
			    	bairro : util.escapeById('inputBairro'),
			    	cep : util.escapeById('inputCep'),
			    	referenciaEndereco : util.escapeById('inputReferenciaEndereco'),
				latitude: util.escapeById('inputLatitude'),
				longitude: util.escapeById('inputLongitude'),
				nome: util.escapeById('inputNome'),
				email: util.escapeById('inputEmail'),
				telefone: util.escapeById('inputTelefone'),
			});
			
			return doacao;
		},
		
		populaEndereco: function(endereco) {
			this.ui.inputEndereco.val(endereco.logradouro);
			this.ui.inputReferenciaEndereco.val(endereco.enderecoFormatado)
			this.ui.inputNumeroEndereco.val(endereco.numero);
			this.ui.inputCep.val(endereco.cep);
			this.ui.inputBairro.val(endereco.bairro);
			this.ui.inputCidade.val(endereco.cidade);
			this.ui.inputUf.val(endereco.uf);
			this.ui.inputLatitude.val(endereco.latitude);
			this.ui.inputLongitude.val(endereco.longitude);
		},
	});
	
	return FormDoacao;
});