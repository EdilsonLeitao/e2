define(function(require) {

	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var DoacaoModel = require('models/DoacaoModel');
	var DoacaoCollection = require('collections/DoacaoCollection');
	var DoacaoPageCollection = require('collections/DoacaoPageCollection');
	var PageDoacaoTemplate = require('text!views/doacao/tpl/PageDoacaoTemplate.html');

	var PageDoacao = Marionette.LayoutView.extend({
		template: _.template(PageDoacaoTemplate),
		
		regions: {
			gridRegion: '#grid',
			counterRegion: '#counter',
			paginatorRegion: '#paginator',
		},
		
		events: {
			'click 	#reset': 'resetDoacao',
			'keypress': 'treatKeypress',
			'click 	.search-button': 'searchDoacao',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
			'click #menu-close': 'toggleSidebarWrapper',
			'click #menu-toggle': 'toggleSidebarWrapper',
		},
		
		ui: {
			inputNome: '#inputNome',
			inputAvancadoNome: 'inputAvancadoNome',
			inputNomeReduzido: '#inputNomeReduzido',
			inputViatura: '#inputViatura',
			inputEmail: '#inputEmail',
			inputDdd: '#inputDdd',
			inputFone: '#inputFone',
			inputStatusDoacao: '#inputStatusDoacao',
			sidebarWrapper: '#sidebar-wrapper',
			form: '#formDoacaoFilter',
			advancedSearchForm: '.advanced-search-form',
			newButton: 'a#new',
			saveButton: 'a#save',
			saveContinueButton: 'a#saveContinue',
		},
		
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		
		toggleSidebarWrapper: function() {
			this.ui.sidebarWrapper.toggleClass("active");
		},

		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchDoacao();
			}
		},
		
		initialize: function() {
			var that = this;
			this.doacao = new DoacaoPageCollection();
			this.grid = new Backgrid.Grid({
				className: 'table backgrid table-striped table-bordered table-hover dataTable no-footer  ',
				columns: this.getColumns(),
				emptyText: "Sem registros",
				collection: this.doacao
			});
			this.counter = new Counter({
				collection: this.doacao,
			});
			this.paginator = new Backgrid.Extension.Paginator({
				columns: this.getColumns(),
				collection: this.doacao,
				className: ' paging_simple_numbers',
				uiClassName: 'pagination',
			});
			this.on('show', function() {
				that.gridRegion.show(that.grid);
				this.searchDoacao();
				that.counterRegion.show(that.counter);
				that.paginatorRegion.show(that.paginator);
				//this.checkButtonAuthority();
			});
		},
		
		searchDoacao: function() {
			var that = this;
			if (!util.escapeById('inputNome')) {
				this.doacao.filterQueryParams = {
				}
			}
			else {
				this.doacao.filterQueryParams = {
					titulo: util.escapeById('inputNome'),
				}
			}
			this.doacao.fetch({
				success: function(_coll, _resp, _opt) {
					console.info('Consulta para o grid doacao');
				},
				error: function(_coll, _resp, _opt) {
					console.log(_resp);
					if(_resp.status == '403'){
						util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
					}
				},
				complete: function() {},
			})
		},
		
		resetDoacao: function() {
			this.ui.form.get(0).reset();
			this.doacao.reset();
		},
		
		getColumns: function() {
			var that = this;
			var columns = [{
				name: "id",
				editable: false,
				sortable: true,
				label: "ID",
				cell: "integer",
			}, {
				name: "titulo",
				editable: false,
				sortable: true,
				label: "Título",
				cell: "string",
			}, {
				name: "latitude",
				editable: false,
				sortable: true,
				label: "Latitude",
				cell: "string",
			}, {
				name: "longitude",
				editable: false,
				sortable: true,
				label: "Longitude",
				cell: "string",
			}, {
				name: "acoes",
				label: "Ações(Editar, Deletar)",
				sortable: false,
				cell: GeneralActionsCell.extend({
					buttons: that.getCellButtons(),
					context: that,
				})
			}];
			return columns;
		},
		
		getCellButtons: function() {
			var that = this;
			var buttons = null;//this.checkGridButtonAuthority();
			return buttons;
		},
		
		verBloqueios: function(model) {
			util.goPage("app/doacao/" + model.get('id') + '/bloqueios');
		},

		verPunicoes: function(model) {
			util.goPage("app/doacao/" + model.get('id') + '/punicaos');
		},

		verVouchers: function(model) {
			util.goPage("app/doacao/" + model.get('id') + '/faixaVouchers');
		},

		deleteModel: function(model) {
			var that = this;
			var modelTipo = new DoacaoModel({
				id: model.id,
			});
			util.Bootbox.confirm("Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?", function(yes) {
				if (yes) {
					modelTipo.destroy({
						success: function() {
							that.doacao.remove(model);
							util.showSuccessMessage('Motoqueiro removido com sucesso!');
						},
						error: function(_model, _resp) {
							util.showErrorMessage('Problema ao remover o registro', _resp);
						}
					});
				}
			});
		},
		
		editModel: function(model) {
			util.goPage("app/editDoacao/" + model.get('id'));
		},

		/*checkGridButtonAuthority: function() {
			var that = this;
			var buttons = [];
			$.grep(roles, function(e) {
				if (e.authority == 'ROLE_BLOQUEIOS' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'bloqueios_button',
						type: 'primary',
						icon: 'fa-ban',
						hint: 'Bloqueios',
						onClick: that.verBloqueios,
					});
				}
				if (e.authority == 'ROLE_PUNICOES' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'punicao_button',
						type: 'primary',
						icon: 'fa-chain-broken',
						hint: 'Punições',
						onClick: that.verPunicoes,
					});
				}
				if (e.authority == 'ROLE_MOTOQUEIROVOUCHER' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'voucher_button',
						type: 'primary',
						icon: 'fa-ticket',
						hint: 'Vouchers',
						onClick: that.verVouchers,
					});
				}
				if (e.authority == 'ROLE_MOTOQUEIRO_EDITAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'edita_ficha_button',
						type: 'primary',
						icon: 'icon-pencil fa-pencil',
						hint: 'Editar Motoqueiro',
						onClick: that.editModel,
					});
				}
				if (e.authority == 'ROLE_MOTOQUEIRO_DELETAR' || e.authority == 'ROLE_ADMIN') {
					buttons.push({
						id: 'delete_button',
						type: 'danger',
						icon: 'icon-trash fa-trash',
						hint: 'Remover Motoqueiro',
						onClick: that.deleteModel,
					});
				}
			})
			return buttons;
		},*/
	});
	
	return PageDoacao;
});