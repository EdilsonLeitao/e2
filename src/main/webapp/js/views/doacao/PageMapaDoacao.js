define(function(require) {

	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');
	var CustomStringCell = require('views/components/CustomStringCell');
	var Counter = require('views/components/Counter');
	var ActionsCell = require('views/components/ActionsCell');
	var GMap = require('views/components/GMap');
	var GeneralActionsCell = require('views/components/GeneralActionsCell');
	var CustomNumberCell = require('views/components/CustomNumberCell');
	var DoacaoModel = require('models/DoacaoModel');
	var DoacaoCollection = require('collections/DoacaoCollection');
	var DoacaoPageCollection = require('collections/DoacaoPageCollection');
	var PageMapaDoacaoTemplate = require('text!views/doacao/tpl/PageMapaDoacaoTemplate.html');
	var ModalSobre = require('views/sobre/ModalSobre');
	var ModalContato = require('views/contato/ModalContato');

	var PageMapaDoacao = Marionette.LayoutView.extend({
		template: _.template(PageMapaDoacaoTemplate),
		regions: {
			modalSobreRegion : '#modalSobre',
			modalContatoRegion : '#modalContato',
		},
		events: {
			'click 	#reset': 'resetDoacao',
			'keypress': 'treatKeypress',
			'click 	.search-button': 'searchDoacao',
			'click .show-advanced-search-button': 'toggleAdvancedForm',
			'click #menu-close': 'toggleSidebarWrapper',
			'click #menu-toggle': 'toggleSidebarWrapper',
			'click #sobre' : 'abrirModalSobre',
			'click #contato' : 'abrirModalContato',
		},
		ui: {
			inputNome: '#inputNome',
			inputViatura: '#inputViatura',
			inputStatusApp: '#inputStatusApp',
			sidebarWrapper: '#sidebar-wrapper',
			mapa : '#map',
			form: '#formDoacaoFilter',
			advancedSearchForm: '.advanced-search-form',
		},
		toggleAdvancedForm: function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},
		toggleSidebarWrapper: function() {
			this.ui.sidebarWrapper.toggleClass("active");
		},
		treatKeypress: function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchDoacao();
			}
		},
		
		initialize: function() {
			var that = this;

			this.doacao = new DoacaoCollection();

			this.modalSobre = new ModalSobre();
			
			this.modalContato = new ModalContato();
			
			this.on('show', function() {
				this.gMap = new GMap({
					mapElement : this.ui.mapa,
				})
				
				this.modalSobreRegion.show(this.modalSobre);
				this.modalContatoRegion.show(this.modalContato);
			});
			this.searchDoacao();
		},

		abrirModalSobre : function() {
			this.modalSobre.showInPage();
		},
		
		abrirModalContato : function() {
			this.modalContato.showInPage();
		},
		
		searchDoacao: function() {
			var that = this;
			//this.doacao.getMapaDoacoes({
			this.doacao.fetch({
				success: function(_coll, _resp, _opt) {
					that.gMap.clear();
					that.gMap.addDoacoes(_coll.toJSON());
					console.info('Consulta para localização doacao');
					console.log(_coll.length);
				},
				error: function(_coll, _resp, _opt) {
					console.log(_resp);
					if(_resp.status == '403'){
						util.showErrorMessage('Você não possui permissão para este recurso. Por favor, contate o administrador do sistema.');
					}
				},
				complete: function() {
					
				},
				/*data : {
					last : false,

					titulo: util.escapeById('inputTitulo'),
				},*/
			})
		},
		resetDoacao: function() {
			this.ui.form.get(0).reset();
			this.doacao.reset();
			this.gMap.clear();
		},
	});
	return PageMapaDoacao;
});