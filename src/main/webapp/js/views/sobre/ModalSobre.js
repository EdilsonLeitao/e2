define(function(require) {

	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Col = require('adapters/col-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var Marionette = require('marionette');
	var Backgrid = require('adapters/backgrid-adapter');
	var util = require('utilities/utils');
	var Combobox = require('views/components/Combobox');


	var ModalSobreTemplate = require('text!views/sobre/tpl/ModalSobreTemplate.html');

	var FormSobre = Marionette.LayoutView.extend({
		template : _.template(ModalSobreTemplate),

		regions : {},

		events : {
		},

		ui : {
			form : '#formSobre',
			modalScreen : '.modal'
		},

		initialize : function() {
			var that = this;

			this.on('show', function() {

			});
		},

		showInPage : function() {
			this.ui.modalScreen.modal({
				show : true,
				backdrop : 'static'
			});
		},
	});

	return FormSobre;
});